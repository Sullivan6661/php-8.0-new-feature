<?php

use Carbon\Carbon;

// attribute (new feature attribute for class or function meta data)
#[Attribute]
class TransactionMeta{}

class Transaction {
    private $productPrice;
    private $motherName;
    private $fatherName;
    
    /**
     *  Constructor (new feature constructor property promotion)
     */
    public function __construct(public $productName = "", public $shippingPrice = "")
    {
        
    }

    /**
     *  get total price
     * 
     *  @return int
     */
    // attribute (new feature attribute for class or function meta data)
    #[TransactionMeta]
    public function getDate()
    {
        return '2022-02-02';
    }

    /**
     *  update profile (new feature named argument)
     * 
     *  @param $moherName, $fatherName
     * 
     *  @return string
     */
    public function setParentName(string $motherName, string $fatherName)
    {
        $this->motherName = $motherName;
        $this->fatherName = $fatherName;
    }

    /**
     *  get parent name
     * 
     *  @return string
     */
    public function getParentName()
    {
        echo 'nama ibu : ' . $this->motherName . ' dan nama ayah :' . $this->fatherName . "\n";
    }

    /**
     *  print number 
     * 
     *  @param int|float $number
     * 
     *  @return void
     */
    public function printNumber(int|float $number)
    {
        echo 'Number: ' . $number . "\n";
    }

    /**
     *  set gender (new feature match expression)
     * 
     *  @param $gender 
     * 
     *  @return string
     */
    public function setGender($gender)
    {
        echo match ($gender) {
            "L" => "Laki - Laki\n",
            "P" => "Perempuan\n"
        };
    }

    /**
     *  string checker 
     * 
     *  @param $string
     * 
     *  @return $string
     */
    public function stringChecker($string)
    {
        if ($string == 42){
            echo 'masuk kondisi' . "\n";
        }
    }

    /**
     *  contain word
     * 
     *  @param $string
     * 
     *  @return void
     */
    public function containWord($string)
    {
        if(str_contains('hello worlds', $string)){
            echo "ada\n";
        } 
    }

    /**
     *  check first and last word
     * 
     *  @param string $string
     * 
     *  @return void
     */
    public function checkFirstAndLastWord($string)
    {
        if (str_starts_with($string, 'hello')) {
            echo "betul\n";
        }

        if (str_ends_with($string, 'words')){
            echo "betul\n";
        }
    }
}

class Foo {
    // new feature weakmap
    private WeakMap $cache;

    public function getSomethingWithCaching(object $obj) : object
    {
        return $this->cache[$obj] ??= $this->computeSomethingExpensive($obj);
    }
}

$transaction = new Transaction('Check', 500);
$date = $transaction?->getDate(); // implement nullsafe operator
$transaction->setParentName(fatherName: 'Jesso', motherName: 'Jessi'); //implement named argument call a parameter name
$transaction->printNumber(12.3); // implement union can input two datatype int and float
$transaction->setGender('P'); // implement match expression
$transaction->stringChecker('42false'); //implement new string to number comparisons ((42 == "42foo) : <php8 = true : php8 = false )
$transaction->containWord('hello'); // implement new php function 
$transaction->checkFirstAndLastWord('hello'); // implemen new php function

